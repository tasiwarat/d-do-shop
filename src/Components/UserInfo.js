import React, { useState, useEffect } from 'react';
import './UserInfo.css';

function UserInfo(props) {
    const [_userinfo, set_userinfo] = useState({
        Id: '',
        Email: ''
    });

    useEffect(() => {
        if (JSON.parse(sessionStorage.getItem('mystorage'))) {
            set_userinfo(JSON.parse(sessionStorage.getItem('mystorage')))
        }
    }, [])

    return (
        <div className="UserInfo_Full">
            <div className="UserInfo_Body">
                <div className="UserInfo_Head">
                    <div className="UserInfo_Head_text">
                        Personal Information
                    </div>
                </div>
                <div className="UserInfo_sect1">
                    <div className="UserInfo_Id">
                        <p>{_userinfo.Id}</p>
                    </div>
                    <div className="UserInfo_Email">
                        <p>{_userinfo.Email}</p>
                    </div>
                </div>
                <div className="UserInfo_sect2">
                    <div className="UserInfo_ButtonClose">
                        <button
                            className="UserInfo_ButtonClose_btn"
                            onClick={() => {
                                props.onClose()
                            }}
                        >
                            Back
                        </button>
                    </div>
                    <div className="UserInfo_ButtonPathHome">
                        <button
                            className="UserInfo_ButtonPathHome_btn"
                            onClick={() => {
                                props.onlogin();
                                window.location.href = '/Home'
                            }}
                        >
                            Log Out
                        </button>
                    </div>
                    <div className="UserInfo_ButtonHistory">
                        <button
                            className="UserInfo_ButtonHistory_btn"
                            onClick={() => {
                                props.onClose()
                                window.location.href = '/History'
                            }}
                        >
                            History
                        </button>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default UserInfo;