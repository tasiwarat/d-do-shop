import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import { Button } from '@material-ui/core';
import './ModalPopupUserHistory.css';

function ModalPopupUserHistory(props) {

    function GenviewDetail() {
        var Genrows = []
        for (let index = 0; index < props.history.length; index++) {
            let Genres = props.history[index]
            Genrows.push(
                <div className="GenviewBody2">
                    <div className="GenviewBody2_2">
                        <img src={Genres.image}></img>
                    </div>
                    <div className="GenviewBody2_1">
                        {Genres.name}
                    </div>
                    <div className="GenviewBody2_1">
                        <div className="Genview_Descrip">{Genres.description}</div>
                    </div>
                    <div className="GenviewBody2_2">
                        {Genres.price}
                    </div>
                    <div className="GenviewBody2_2">
                        {Genres.summary}
                    </div>
                </div>
            )
        }
        return (Genrows)
    }

    return (
        <Modal
            open={props.Modalvisible}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <div className="GenviewDetail">

                <div className="GenviewBody">
                    <div className="GenviewBody1">
                        <div className="GenviewBody1_2">
                            Image
                        </div>
                        <div className="GenviewBody1_1">
                            Name
                        </div>
                        <div className="GenviewBody1_1">
                            <div className="Genview_Descrip">Description</div>
                        </div>
                        <div className="GenviewBody1_2">
                            Price
                        </div>
                        <div className="GenviewBody1_2">
                            Sum
                        </div>
                    </div>
                    {GenviewDetail()}
                    <div className="GenviewButtonClose">
                        <button
                            className="Genview_close_btn"
                            onClick={() => {
                                props.onclose()
                            }}
                        >
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}
export default ModalPopupUserHistory;