import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import './Regist.css';
import Postregist from '../Api/Postregist';
import Modal from '@material-ui/core/Modal';
import './ModalPopupUserHistory.css';

function Regist() {
    const [Registid, setRegistid] = useState("")
    const [Registpass, setRegistpass] = useState("")
    const [Registemail, setRegistemail] = useState("")

    function getregistid(val) {
        setRegistid(val.target.value)
    }
    function getregistpass(val) {
        setRegistpass(val.target.value)
    }
    function getregistemail(val) {
        setRegistemail(val.target.value)
    }
    function onregist() {
        if (Registid.length == 0 || Registpass.length == 0 || Registemail.length == 0) {
            alert("Can't Regist")
        } else {
            Postregist(Registid, Registpass, Registemail).then((res) => {
                window.location.href = '/Home'
                alert("Regist Success")
            }).catch((err) => {
                alert("Can't Regist")
            })
        }
    }

    return (
        <Modal
            open={true}
        >
            <div className="regist_full">
                <div className="regist_show">
                    <div className="regist_left">
                        <img src="/Images/Picture_WorkPerson.jpg"></img>
                    </div>
                    <div className="regist_right">
                        <div className="regist_header">
                            D-DO Shop
                        </div>
                        <div className="regist_subtitle">
                            Register
                        </div>
                        <div className="regist_text_id">
                            <TextField
                                id="standard-password-input"
                                label="Id"
                                type="id"
                                fullWidth={true}
                                autoComplete="current-password"
                                value={Registid}
                                onChange={getregistid}
                            />
                        </div>
                        <div className="regist_text_password">
                            <TextField
                                id="standard-password-input"
                                label="Password"
                                type="password"
                                fullWidth={true}
                                color="primary"
                                autoComplete="current-password"
                                value={Registpass}
                                onChange={getregistpass}
                            />
                        </div>
                        <div className="regist_text_email">
                            <TextField
                                id="standard-password-input"
                                label="Email"
                                type="Email"
                                fullWidth={true}
                                autoComplete="current-password"
                                value={Registemail}
                                onChange={getregistemail}
                            />
                        </div>
                        <div className="regist_button_submit">
                            <Button
                                variant="contained"
                                color="Secondary"
                                fullWidth={true}
                                onClick={() => { onregist() }}>
                                Regist
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}
export default Regist;