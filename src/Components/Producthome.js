import React, { useState, useEffect } from 'react';
import Pic01 from '../PicSlide/Pic01';
import './Producthome.css';
import PostProduct from '../Api/PostProduct';
import Cart_btn from './Cart_btn';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

function Producthome() {
    const [ProductApi, setProductApi] = useState([]);
    const [CartOrder, setCartOrder] = useState([]);
    const [SummaryCount, setSummaryCount] = useState(0);
    const [SummaryPrice, setSummaryPrice] = useState(0);
    const [_DataSearch, set_DataSearch] = useState("");
    const [_DataCreate, set_DataCreate] = useState([]);

    useEffect(() => {
        PostProduct("").then((res) => {
            console.log(res)
            setProductApi(res.data.data)
            set_DataCreate(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [])
    useEffect(() => {
        SummaryCartProduct()
    }, [CartOrder])

    function SummaryCartProduct() {
        var Summary_count = 0;
        var Summary_price = 0;
        for (let i = 0; i < CartOrder.length; i++) {
            let _item = CartOrder[i];
            Summary_count = Summary_count + _item.count;
            Summary_price = Summary_price + (_item.count * _item.price);
        }
        setSummaryCount(Summary_count)
        setSummaryPrice(Summary_price)
        sessionStorage.setItem('SumCount', (Summary_count));
        sessionStorage.setItem('SumPrice', (Summary_price));
    }
    function SessionCart() {
        sessionStorage.setItem('SCartOrder', JSON.stringify(CartOrder));
    }
    function DataSearch_Get(val) {
        set_DataSearch(val.target.value)
    }
    function DataSearch_btn() {
        if (ProductApi.length > 0) {
            if (_DataSearch == "") {
                set_DataCreate(ProductApi)
            } else {
                set_DataCreate(ProductApi.filter(res => (res.name).toLowerCase().includes(_DataSearch)))
            }
        } else {

        }
    }

    return (
        <div className="Product_fullpage">
            <div className="Product_header">
                <div className="Product_header_sect1">
                    <Input
                        fullWidth={true}
                        placeholder="Search Name..."
                        id="input-with-icon-adornment"
                        value={_DataSearch}
                        onChange={DataSearch_Get}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={() => { DataSearch_btn() }}
                                    type="submit"
                                    aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </div>
            </div>
            <div className="Product_body">
                {
                    _DataCreate.map(res =>
                    (<Pic01
                        colorbg={"#1f3933"}
                        widthbg={"50vw"}
                        heightbg={"40vh"}
                        margintopbg={"20px"}
                        borderbg={"5px"}
                        colortext={"#fff"}
                        dataItem={res}
                        onClick={() => {
                            if (CartOrder.filter(ress => ress.index === res.index).length !== 0) {
                                var dataGen = []
                                for (let j = 0; j < CartOrder.length; j++) {
                                    var resscheck = CartOrder[j];
                                    if (res.index == resscheck.index) {
                                        let _count = resscheck.count + 1;
                                        dataGen.push({
                                            Status: resscheck.Status,
                                            UpdateAt: resscheck.UpdateAt,
                                            count: _count,
                                            description: resscheck.description,
                                            image: resscheck.image,
                                            index: resscheck.index,
                                            name: resscheck.name,
                                            option: resscheck.option,
                                            price: resscheck.price,
                                            quality: resscheck.quality
                                        });
                                    } else {
                                        dataGen.push(resscheck);
                                    }
                                }
                                setCartOrder(dataGen);
                                console.log(dataGen)
                            } else {
                                res.count = 1;
                                let dataGen = [...CartOrder, res];
                                console.log(dataGen)
                                setCartOrder(dataGen);
                            }
                        }}
                    />)
                    )
                }
                <div className="product_bottom"></div>
            </div>
            <Cart_btn
                sumcount={SummaryCount}
                sumprice={SummaryPrice}
                onCik={() => SessionCart()} />
        </div>
    )
}
export default Producthome;