import React, { useState } from 'react';
import './Cart.css';
import { Link } from "react-router-dom";
import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PostBooking from '../Api/PostBooking';

function Cart() {
    const [Open, setOpen] = useState(false);
    function Cart_Close() {
        setOpen(false);
    }
    function Productname() {
        return JSON.parse(sessionStorage.getItem('SCartOrder')).map(item =>
        (

            <div className="showsession">
                <div className="showsession_sect1">
                    <img src={item.image}></img>
                </div>
                <div className="showsession_sect2">
                    <div className="showsession_sect2_1">
                        <div className="showsession_sect2_2_Descrip">{item.name}</div>
                    </div>
                    <div className="showsession_sect2_2">
                        <div className="showsession_sect2_2_Descrip">{item.description}</div>
                    </div>
                </div>
                <div className="showsession_sect3">
                    <div>{item.count}</div>
                </div>
                <div className="showsession_sect4">
                    <div>{item.price * item.count}</div>
                </div>
            </div>
        )
        )
    }
    function SumCart() {
        var _SumCart = 0
        var _SumCartItem = JSON.parse(sessionStorage.getItem('SCartOrder'));
        for (let i = 0; i < _SumCartItem.length; i++) {
            let _Item = _SumCartItem[i]
            _SumCart = _SumCart + (_Item.count * _Item.price)
        }
        return (_SumCart)
    }
    function CartBuy() {
        var _CartBuy_Cart = JSON.parse(sessionStorage.getItem('SCartOrder'));
        var _CartBuy_person = JSON.parse(sessionStorage.getItem('mystorage'));
        var _CartBuy_rows = []
        var _CartBuy_Sum = 0
        for (let j = 0; j < _CartBuy_Cart.length; j++) {
            let _CartBuy_Cart_index = _CartBuy_Cart[j].index;
            let _CartBuy_Cart_count = _CartBuy_Cart[j].count;
            let _CartBuy_Cart_price = _CartBuy_Cart[j].price;
            let _CartBuy_Cart_Sumprice = _CartBuy_Cart_price * _CartBuy_Cart_count
            _CartBuy_Sum = _CartBuy_Sum + _CartBuy_Cart_Sumprice
            _CartBuy_rows.push({
                "product_id": _CartBuy_Cart_index,
                "quality": _CartBuy_Cart_count,
            })
        }
        var _CartBuy_rowscart = {
            dataBooking: JSON.stringify(_CartBuy_rows),
            "user": _CartBuy_person.Id,
            "price": _CartBuy_Sum,
        }
        console.log(_CartBuy_rowscart);
        {
            PostBooking(_CartBuy_rowscart).then((res) => {
                console.log(res);
                setOpen(true);
                sessionStorage.removeItem('SCartOrder');
            }).catch((err) => {
                console.log(err);
                setOpen(true);
            })
        }
    }
    return (
        <div className="fullcart">
            <div className="Cart_top">
                <div className="Cart_top_head">
                    <div className="Cart_top_text">My Cart</div>
                </div>
            </div>
            <div className="Cart_Show">
                <div className="Cart_body">
                    <div className="Cart_body_sect1">
                        <div>Image</div>
                    </div>
                    <div className="Cart_body_sect2">
                        <div>Product</div>
                    </div>
                    <div className="Cart_body_sect3">
                        <div>Count</div>
                    </div>
                    <div className="Cart_body_sect4">
                        <div>Price</div>
                    </div>
                </div>
                {Productname()}
                <div className="Cart_footer">
                    <div className="Cart_footer_back">
                        <button
                            className="Cart_footer_btn_back"
                            onClick={() => {
                                window.location.href = '/Product'
                            }}
                        >
                            Back
                        </button>
                    </div>
                    <div className="Cart_footer_sum">
                        <p>Total: {SumCart()} Baht</p>
                    </div>
                    <div className="Cart_footer_buy">
                        <button
                            className="Cart_btn_buy"
                            onClick={() => {
                                CartBuy();
                            }}
                        >
                            Buy
                        </button>
                    </div>
                </div>
                <Dialog
                    open={Open}
                    onClose={setOpen}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"ผลการซื้อสินค้า"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h1>Success</h1>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Link to='/Product'>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    Cart_Close();

                                }} color="primary">
                                <p>Retrun to Product</p>
                            </Button>
                        </Link>
                    </DialogActions>
                </Dialog>
            </div>
        </div>
    );
}
export default Cart;