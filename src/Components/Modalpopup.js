import React, { useState } from 'react';
import Modal_Popup from './Popup';
import Modal from '@material-ui/core/Modal';
import UserInfo from './UserInfo';
import './Modalpopup.css';


function Modalpopup(props) {
    const [_Modalrequest, set_Modalrequest] = useState(false)
    return (
        <Modal
            open={props.Modalvisible}
            onRequestClose={set_Modalrequest}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            {props.ModalType === "Login" ?
                <Modal_Popup
                    onlogin={(reslogin) => {
                        props.onlogin(reslogin)
                    }}
                    onClose1234={() => {
                        props.onClosevisit();
                    }} />
                :
                <UserInfo
                    onlogin={(reslogin) => {
                        props.onlogin(reslogin)
                    }}
                    onClose={() => {
                        props.onClosevisit();
                    }} />
            }
        </Modal>
    )
}
export default Modalpopup;