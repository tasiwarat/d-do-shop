import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import { Postlogin } from '../Api';
import {
    Link
} from "react-router-dom";

import TextField from '@material-ui/core/TextField';
import FacebookIcon from '@material-ui/icons/Facebook';
import './Popup.css';


function Modal_Popup(props) {

    const [data, setData] = useState("")
    const [datap, setDatap] = useState("")

    function getData(val) {
        setData(val.target.value)
        console.log(val.target.value)
    }
    function getDatap(val) {
        setDatap(val.target.value)
        console.log(val.target.value)
    }
    function onlogin() {
        console.log(data, datap)
        Postlogin(data, datap).then((res) => {
            console.log(res);
            props.onClose1234();
            sessionStorage.setItem('mystorage', JSON.stringify(res.data.data[0]));
            props.onlogin(res.data.data)
            window.location.href = '/Home';
        }).catch((err) => {
            console.log(err);
        })
    }
    function onregist() {
        props.onClose1234();
        window.location.href = '/Regist';
    }

    return (
        <div className="Modal_component">
            <div className="Modal_component_textp">
                <p>Login</p>
            </div>
            <div className="Modal_component_TextField1">
                <TextField
                    id="outlined-secondary"
                    label="Username"
                    variant="outlined"
                    color="secondary"
                    fullWidth={true}
                    value={data}
                    onChange={getData}
                />
            </div>
            <div className="Modal_component_TextField2">
                <TextField
                    id="outlined-secondary"
                    label="Password"
                    type="password"
                    fullWidth={true}
                    variant="outlined"
                    color="secondary"
                    value={datap}
                    onChange={getDatap}
                />
            </div>
            <div className="Modal_component_button1">
                <button
                    className="Modal_login_btn"
                    onClick={() => {
                        onlogin();
                    }}
                >
                    Login
                </button>
            </div>
            <div div className="Modal_component_button2">
                <button
                    className="Modal_regist_btn"
                    onClick={() => {
                        onregist();
                    }}
                >
                    Regist
                </button>
            </div>
            <div>
                <p>Or Sign in</p>
            </div>
            <div>
                <FacebookIcon />

            </div>
        </div>

    );
}
export default Modal_Popup;