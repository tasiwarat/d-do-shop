import React from 'react';
import './Cart_btn.css';

function Cart_btn(props) {

    return (
        <div className="header">
            <div className="cartbutton">
                <button
                    className="btn_Cartbtn"
                    onClick={() => {
                        props.onCik()
                        window.location.href = '/Cart'
                    }}
                >
                    Click to Cart
                </button>
            </div>
            <div className="count_p">
                <p>Count: {props.sumcount}</p>
            </div>
            <div className="count_p">
                <p>Price: {props.sumprice} </p>
            </div>
        </div>
    )
}
export default Cart_btn;