import React, { useEffect, useState } from 'react';
import GetUserHistory from '../Api/GetUserHistory';
import GetUserHistoryRef from '../Api/GetUserHistoryRef';
import ModalPopupUserHistory from './ModalPopupUserHistory';
import './UserHistory.css';
import { Button } from '@material-ui/core';
import Moment from 'react-moment';

function UserHistory() {
    const [_ModalOpen, set_ModalOpen] = useState(false)
    const [_History, set_History] = useState([])
    const [_HistoryRef, set_HistoryRef] = useState([])
    useEffect(() => {
        GetUserHistory(JSON.parse(sessionStorage.getItem('mystorage')).Id).then((res) => {
            console.log(res)
            set_History(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    function ProductHistory() {
        var ProductHistory_rows = []
        for (let index = 0; index < _History.length; index++) {
            let res = _History[index];
            ProductHistory_rows.push(
                <div className="UserHistory_Fullsect">
                    <div className="UserHistory_sect1">
                        {res.RefId}
                    </div>
                    <div className="UserHistory_sect2">
                        {res.Price}
                    </div>
                    <div className="UserHistory_sect3">
                        <Moment date={new Date(res.CreateDate)} format="YYYY MMM DD HH:mm" />
                    </div>
                    <div className="UserHistory_sect4">
                        <button
                            className="UserHisotry_btn_detail"
                            onClick={() => {
                                GetUserHistoryRef(res.RefId).then((res) => {
                                    console.log(res);
                                    set_HistoryRef(res.data.data)
                                    set_ModalOpen(true)
                                }).catch((err) => {
                                    console.log(err);
                                })
                            }}
                        >
                            Detail
                        </button>
                    </div>
                </div>
            )
        }
        return (ProductHistory_rows)
    }

    return (
        <div className="UserHistory_Full">
            <div className="UserHistory_show">
                <div className="UserHistory_Head_Topic">
                    <div className="UserHistory_Topic">History</div>
                </div>
                <div className="UserHistory_Table_Head">
                    <div className="UserHistory_Table_Head1">
                        Ref
                    </div>
                    <div className="UserHistory_Table_Head2">
                        Price
                    </div>
                    <div className="UserHistory_Table_Head2">
                        Createdata
                    </div>
                    <div className="UserHistory_Table_Head2">
                        Detail
                    </div>
                </div>
                {ProductHistory()}

                <ModalPopupUserHistory

                    Modalvisible={_ModalOpen}
                    onclose={() => {
                        set_ModalOpen(false)
                    }}
                    history={_HistoryRef}
                />
            </div>
        </div>
    )
}
export default UserHistory;