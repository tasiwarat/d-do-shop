import React from 'react';
import { Link } from 'react-router-dom'
import { Button } from '@material-ui/core';
import { useEffect } from 'react';
import './UserInfo_btn.css';
import '../Global.css';

function UserInfo_btn(props) {
    useEffect(() => {
        console.log(props)
    }, [])
    return (
        <div>
            <button
                className="Userbtn"
                component={Link}
                onClick={() => {
                    props.onClickBTN();
                }}
            >
                {props.user.Id ? props.user.Id : "Login"}
            </button>
        </div>
    )
}
export default UserInfo_btn;