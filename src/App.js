import './App.css';
import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Contact from './Contact';
import Home from './Home';
import Regist from './Components/Regist';
import Header from './Header';
import Producthome from './Components/Producthome';
import Cart from './Components/Cart';
import UserInfo from './Components/UserInfo';
import UserHistory from './Components/UserHistory';

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path="/Home">
            <Home />
          </Route>
          <Route path="/Contact">
            <Contact />
          </Route>
          <Route path="/Regist">
            <Regist />
          </Route>
          <Route path="/Product">
            <Producthome />
          </Route>
          <Route path="/Cart">
            <Cart />
          </Route>
          <Route path="/UserInfo">
            <UserInfo />
          </Route>
          <Route path="/History">
            <UserHistory />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
export default App;