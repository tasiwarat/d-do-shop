import React, { Component } from 'react';
import './Contact.css';
import CallIcon from '@material-ui/icons/Call';
import IconButton from '@material-ui/core/IconButton';
import MailIcon from '@material-ui/icons/Mail';
import FacebookIcon from '@material-ui/icons/Facebook';
import './Global.css';

class Contact extends Component {
    render() {
        return (
            <div className="Contact_Fullpage">
                <div className="Contact_Body_Sect1">
                    <div className="Contact_Topic">Contact Page</div>
                </div>
                <div className="Contact_Show">
                    <div className="Contact_Body">
                        <IconButton>
                            <CallIcon
                                color="primary"
                            />
                        </IconButton>
                        <p>
                            Please Tell: 092-554-5910
                        </p>
                    </div>
                    <div className="Contact_Body">
                        <IconButton>
                            <MailIcon
                                color="primary"
                            />
                        </IconButton>
                        <p>
                            Gmail: tasiwarat@gmail.com
                        </p>
                    </div>
                    <div className="Contact_Body">
                        <IconButton>
                            <FacebookIcon
                                color="primary"
                            />
                        </IconButton>
                        <p>
                            Facebook: https://web.facebook.com/profile.php?id=100024661131301
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}
export default Contact;