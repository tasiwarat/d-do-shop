
import React from 'react';
import axios from 'axios';

function GetUserHistoryRef(Ref) {
    return new Promise ((resolve, reject) =>{
        axios.get('http://localhost:5000/TransectionDetail?Ref='+Ref).then(res => {
            console.log(res);
            resolve(res);
        }).catch(err => {
            console.log(err.response.data.message);
            reject(err);
        })
    }
    )
}
export default GetUserHistoryRef;