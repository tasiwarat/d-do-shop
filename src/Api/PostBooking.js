import axios from 'axios';

function PostBooking(dataBooking) {

    return new Promise((resolve, reject) => {
        console.log(dataBooking);
        axios.post('http://localhost:5000/Booking', dataBooking
        ).then(res => {
            console.log(res);
            resolve(res)
        }).catch(err => {
            console.log(err.response.data.message);
            reject(err)
        })
    })
}
export default PostBooking;