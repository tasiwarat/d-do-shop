import React, { useEffect, useState } from 'react';
import 'react-slideshow-image/dist/styles.css';
import Picslide from './PicSlide/picslide';
import './Home.css';
import './Global.css';

function Home() {

    return (
        <div className="Home_Full">
            <div className="Home_Body">
                <div className="Home_Body_Sect1">
                    <div className="Header_Global_h2">Best Seller</div>
                </div>
                <div className="Home_Body_Sect2">
                    <Picslide />
                </div>
            </div>
        </div>
    );
}

export default Home;