import React, { useEffect, useState } from 'react';
import { Link, useHistory } from "react-router-dom";
import UserInfo_btn from './Components/UserInfo_btn';
import './Header.css';
import Modalpopup from './Components/Modalpopup';
import './Global.css';

function Header(props) {
    const [_chagenclassname, set_changeclassname] = useState([])
    const pathname = window.location.pathname
    const objMenu = [
        {
            index: 0,
            className: "Header_Body_Sect2_1",
            title: "Home",
            active: 1
        },
        {
            index: 1,
            className: "Header_Body_Sect2_1",
            title: "Contact",
            active: 0
        },
        {
            index: 2,
            className: "Header_Body_Sect2_1",
            title: "Product",
            active: 0
        }
    ]

    useEffect(() => {

        const GenMenu = [];
        objMenu.forEach(res => {
            var _active = 0;
            if (pathname == "/" + res.title) {
                _active = 1;
            }
            GenMenu.push({
                index: res.index,
                className: res.className,
                title: res.title,
                active: _active
            })
        })
        set_changeclassname(GenMenu)
        console.log(pathname);
    }, [])

    const [visiLogin, set_visiLogin] = useState(false);
    const [_User, setUser] = useState([]);
    const [visiLogout, set_visiLogout] = useState(false);
    const [_step, set_step] = useState(0);
    const _userhistory = useHistory()
    useEffect(() => {
        if (JSON.parse(sessionStorage.getItem('mystorage'))) {
            set_visiLogin(false)
            setUser(JSON.parse(sessionStorage.getItem('mystorage')))
            set_step(1);
        } else if (pathname == "/Regist") {
            set_visiLogin(false)
        } else {
            set_step(0);
            set_visiLogin(true);
        }
    }, [])
    useEffect(() => {
        console.log(props);
    }, [])

    return (
        <div className="head">
            <div className="header_Body_sect1">
                <div>D-DO Shop</div>
            </div>
            <div className="header_Body_sect2">
                {_chagenclassname.map(res => {
                    return (
                        <div className="header_row">
                            <div className="header_text">
                                <div to={"/" + res.title}
                                    onClick={() => {
                                        window.location = "/" + res.title;
                                    }}
                                    className={res.active == 1 ? "Header_Body_Sect2_2" : "Header_Body_Sect2_1"}
                                >
                                    {res.title}
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className="Header_Body_sect3">
                <UserInfo_btn

                    onClickBTN={() => {
                        console.log("Step: " + _step)
                        if (_step == 1) {
                            set_visiLogout(true);
                        } else {
                            set_visiLogin(true);
                        }
                    }}
                    user={_User}
                />
            </div>
            <Modalpopup
                Modalvisible={visiLogout}
                onlogin={() => {
                    set_visiLogout(false);
                    sessionStorage.removeItem('mystorage');
                    setUser([]);
                    set_step(0);
                    _userhistory.push('/Home', null);
                    set_visiLogin(true);
                }}
                onClosevisit={() => {
                    set_visiLogout(false);
                }}
                ModalType={"Logout"}
            />
            <Modalpopup
                Modalvisible={visiLogin}
                onlogin={(reslogin) => {
                    console.log(reslogin);
                    set_visiLogin(false);
                    setUser(reslogin[0]);
                    set_step(1);
                }}
                onClosevisit={() => {
                    set_visiLogin(false)
                }}
                ModalType={"Login"}
            />
        </div>
    );
}
export default Header;