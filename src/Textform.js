import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import { Postlogin } from './Api';




function Textform () {



    const [data, setData] = useState(null)
    const [datap, setDatap] = useState(null)
    const [show, setShow] = useState(false)

    function getData(val) {
        setData(val.target.value)
        console.log(val.target.value)

    }

    function getDatap(val) {
        setDatap(val.target.value)
        console.log(val.target.value)
    }

    function onlogin() {
        console.log(data, datap)
        Postlogin(data, datap).then((res) => {
            console.log(res)
        }).catch((err) => {
            console.log(err)
        })
    }

    return (

        <div>



        <TextField
        id="outlined-secondary"
        label="ID"
        variant="outlined"
        color="secondary"
        value={data}
        onChange={getData}
        />
        
        <TextField
        id="outlined-secondary"
        label="Password"
        variant="outlined"
        color="secondary"
        value={datap}
        onChange={getDatap}
        />

        <Button variant="contained" color="Primary" onClick={() => onlogin()}>Login</Button>



        </div>



    )


}

export default Textform;