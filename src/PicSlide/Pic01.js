import React, { useState, useEffect } from 'react';
import './Pic01.css';
import '../Global.css';

function Pic01(props) {
    const [_stylebox, set_stylebox] = useState({
        display: "",
        width: "",
        height: "",
        backgroundColor: "",
        borderRadius: "",
        marginTop: "",
        color: "",
    });
    useEffect(() => {
        set_stylebox({
            display: "flex",
            width: props.widthbg,
            height: props.heightbg,
            backgroundColor: props.colorbg,
            borderRadius: props.borderbg,
            marginTop: props.margintopbg,
            color: props.colortext,
        })
    }, []);

    return (
        <div className="pic">
            <div style={_stylebox}>
                <div className="Pic_Div_01">
                    <img src={props.dataItem.image}></img>
                </div>
                <div className="pic_slide">
                    <div className="pic_slide01">
                        <div className="pic_name">{props.dataItem.name}</div>
                        <div className="pic_option">
                            Option: {props.dataItem.option}
                        </div>
                        <div className="pic_price">฿{props.dataItem.price}</div>
                    </div>
                    <div className="pic_btnaddcart">
                        <button
                            className="btn_product"
                            onClick={() => {
                                props.onClick();
                            }}
                        >
                            Add to Cart
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Pic01;