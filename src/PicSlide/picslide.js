import React, { useState, useEffect } from 'react';
import { Slide } from "react-slideshow-image";
import 'react-slideshow-image/dist/styles.css';
import './picslide.css';

function Picslide() {
    const [objdata, setObjdata] = useState([]);

    useEffect(() => {
        var objtest = [];
        for (let index = 1; index < 4; index++) {
            objtest.push({
                index: index,
                name: "D-Do Product" + index.toString(),
                image: "/Images/picture" + index.toString() + ".jpg",
                price: 123400,
                option: "ice",
            })
        }
        setObjdata(objtest)
    }, [])

    return (
        <div className="PicSlide_Full">
            <div className="PicSlide_Body">
                <Slide
                >
                    {
                        objdata.map(res => {
                            return (
                                <div className="Picslide_pic">
                                    <div className="Picslide_pic_left">
                                        <div className="Picslide_pic_img">
                                            <img src={res.image}></img>
                                        </div>
                                    </div>
                                    <div className="Picslide_pic_right">
                                        <div className="Picslide_name">
                                            <div className="Picslide_name_text">
                                                {res.name}
                                            </div>
                                        </div>
                                        <div className="Picslide_option">
                                            <div className="Picslide_text">
                                                Option: {res.option}
                                            </div>
                                        </div>
                                        <div className="Picslide_price">
                                            <div className="Picslide_price_text">
                                                ฿{res.price}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </Slide>
            </div>
        </div>
    );
}
export default Picslide;